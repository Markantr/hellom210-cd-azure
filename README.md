# Hellom210 CD - Beispiel Kubernetes in Azure

Damit wir die Webanwendung hellom210 in Kubernetes betreiben können benötigt es im Minimum drei Dienste. Ein Deployment, ein Service und ein Ingress-Controller.

Das Deployment erstellt die Pods. Ähnlich wie bei Docker die Container. Häufig wird dazu ein Docker-Image verwendet.

Der Service macht die Pods im Cluster verfügbar damit andere Dienste auf die Pods zugreifen können. Es ist quasi wie ein Mini-Gateway der die Verbindungen zu den unterschiedlichen Pods managed.

Der Ingress-Controller sorgt dafür, dass ein Service extern im Netzwerk verfügbar ist. Es öffnet also ein Port auf der K8s-Cluster IP auf dem wir dann auf den Dienst zugreiffen können.

Da wir nun in der Azure-Cloud sind müssen wir explizit einen Ingress-Controller installieren. Am flexibelsten und am einfachsten zu nutzen ist der NGINX-Ingress Controller. Wir müssen diese mit HELM installieren um diesen verwenden zu können. Dieser Controller konfiguriert Azure dann automatisch mit einer externen IP um auf unsere Anwendung zugreifen zu können.

## Den Dienst in Azure K8s mit Ingress-Controller starten mit YML-Files

- Logge dich in Azure ein und verbinde dich mit Kubernetes. Falls du mehrere Tenants hast gebe den richtigen Tenant noch zusätzlich an.  
  `az login`
  `az login --tenant <UUID>`
- Verbinde dich mit dem Kubernetes. Dies stellt den Kontext von kubectl automatisch auf die K8s-Instanz sodass alle Befehle da ausgeführt werden.  
  `az aks get-credentials --resource-group rgK8s --name aksK8s`

- Installiere den NGINX-Ingress Controller

  ```
  helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
  helm repo update

  kubectl create namespace ingress-nginx

  helm install nginx-ingress ingress-nginx/ingress-nginx --namespace ingress-nginx

  ```

- Den Namespace für die Applikation erstellen und so einstellen, dass wir automatisch in diesem Namespace arbeiten.  
  `kubectl create namespace hellom210`  
  `kubectl config set-context --current --namespace=hellom210`

- Gitlab Container Registry verbinden damit wir das Image pullen können.  
  `kubectl create secret docker-registry gitlab-registry-secret --docker-server=registry.gitlab.com --docker-username=<username> --docker-password=<acces-token>`

- Pods erstellen:
  `kubectl apply -f hellom210-deployment.yml`

- Service erstellen:
  `kubectl apply -f hellom210-service.yml`

- Ingress-Controller erstellen:
  `kubectl apply -f hellom210-ingress.yml`

- K8s Ingress öffentliche IP anzeigen:
  `kubectl get ingress`

- Teste ob du auf die Webapp zugreifen kannst.
  `curl http://<minikubeip>`

## Den Dienst in K8s starten mit ArgoCD

- Argos sollte bereits installiert sein, wenn nicht hier nochmals kurz die Schritte. Installieren und einloggen. Achte darauf, dass du in einem zweiten Terminal den PortForward auf die Instanz in Azure machst.

  ```
  kubectl create namespace argocd
  kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
  kubectl port-forward svc/argocd-server -n argocd 8080:443 #In separaten Terminal
  kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d && echo
  argocd login localhost:8080 --username admin --password <siehe Befehl vorher>
  ```

- Unser Gitlab Repository hinzufügen. Zuerst Access Token erstellen.
  `argocd repo add https://gitlab.com/dein-repository.git --username YOUR_GITLAB_USERNAME --password YOUR_PERSONAL_ACCESS_TOKEN`

- Den Namespace für die Applikation erstellen
  `kubectl create namespace hellom210`

- Die Applikation in ArgosCD erstellen
  `argocd app create hellom210 --repo https://gitlab.com/dein-repository.git --path . --dest-server https://kubernetes.default.svc --dest-namespace hellom210`

- Die Applikation mit einem Sync starten
  `argocd app sync hellom210`

- Den Sync automatisieren (alle 3 Minuten sync ist default)
  `argocd app set hellom210 --sync-policy automated`
